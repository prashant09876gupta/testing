<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order report Details</title>
</head>
<body>
<center>
	<h2>Order Report</h2>
	<form:form modelAttribute="ReportWithinRange" method="POST" action="${pageContext.request.contextPath }/OrderDetailsWithinRangeDate.html">
			<table border="2">
				<tr>
					<td>From Price:<form:input path="fromPrice" /> </td>
					<td>To Price:<form:input path="toPrice" /> </td>
				</tr>	
			</table>
			<br>
			<input type="submit" value="Fetch Details">
			<br>
			<br>
			<br>
			<c:if test="${not empty OrderBill}">
				
				<table border="2">
				<tr>
					<th>Order Id</th><th>Customer Name</th><th>PizzaId</th><th>Bill</th><th>Quantity</th>
				</tr>
				
				<c:forEach items="${OrderBill}" var="item">
				<tr>
					<td><c:out value="${item.orderId}"/> </td>
					<td><c:out value="${item.customerName}"/> </td>
					<td><c:out value="${item.pizzaId}"/></td>
					<td> <fmt:formatNumber value="${item.bill}" type="currency" currencySymbol="INR." ></fmt:formatNumber>
					</td>
					<td><c:out value="${item.numberOfPiecesOrdered}"/></td>
				</tr>
				</c:forEach>
				
			</table>
			
			</c:if>
			
			<a href="${pageContext.request.contextPath}/index.jsp">Home</a>
	
	
	
	</form:form>


</center>

</body>
</html>