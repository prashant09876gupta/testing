package com.accenture.lkm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.lkm.bussiness.bean.BillRangeBean;
import com.accenture.lkm.bussiness.bean.PizzaBean;
import com.accenture.lkm.bussiness.bean.PizzaOrderBean;
import com.accenture.lkm.entity.PizzaEntity;
import com.accenture.lkm.entity.PizzaOrderEntity;

@Repository
@Transactional("txManager")
public class PizzaDaoWrapper {
	
	@Autowired
	private PizzaDAO pizzaDAO;
	@Autowired
	private PizzaOrderDAO pizzaOrderDAO;
	@PersistenceContext
	EntityManager entityManager;
	 
	
	
	public List<PizzaBean> findAllPizzaDetails() throws Exception
	{
		List<PizzaBean>pizza=new ArrayList<PizzaBean>();
		try {
			List<PizzaEntity>listEntity= pizzaDAO.findAllPizzaDetails();
			
			
			for (PizzaEntity pizzaEntity : listEntity) {
				PizzaBean pizzaBean =  convertEntityToBean(pizzaEntity);  
				pizza.add(pizzaBean);
			}
			
		}
		catch(Exception e)
		{
			throw e;
		}
		return pizza;
		
	}
	
	public PizzaBean convertEntityToBean(PizzaEntity entity) throws Exception{

		PizzaBean pizza=new PizzaBean();
		BeanUtils.copyProperties(entity, pizza);
		return pizza;
	}
	public PizzaOrderBean addPizzaOrderDetails(PizzaOrderBean pizzaOrderBean) throws Exception{
		PizzaOrderBean pizzaOrder = null;
		PizzaOrderEntity pizzaOrderEntity =convertPizzaOrderBeanToEntity(pizzaOrderBean); 
		try {
				
			pizzaOrder = convertPizzaOrderEntityToBean(pizzaOrderDAO.save(pizzaOrderEntity));
		} catch (Exception exception) {
			throw exception;
		}
		return pizzaOrder;
	}
	
	
	private PizzaOrderEntity convertPizzaOrderBeanToEntity(PizzaOrderBean pizzaOrderBean) {

		PizzaOrderEntity pizzaEntity=new PizzaOrderEntity();
		BeanUtils.copyProperties(pizzaOrderBean, pizzaEntity);
		return pizzaEntity;
	}
	public PizzaOrderEntity convertBeanToEntity(PizzaOrderBean pizzaOrder)throws Exception {
		// TODO Auto-generated method stub
		PizzaOrderEntity pizzaEntity=new PizzaOrderEntity();
		BeanUtils.copyProperties(pizzaOrder,pizzaEntity);
		return pizzaEntity;
	}
	public Double getPizzaPrice(Integer pizzaId)
	{
		double price=0;
		PizzaEntity pizzaEntity=entityManager.find(PizzaEntity.class, pizzaId);
		if(pizzaEntity!=null){
			price=pizzaEntity.getPrice();
		}
		return price;
		
	}
	public List<PizzaOrderBean> getOrderDetails(Double fromBill, Double toBill )
	{
		List<PizzaOrderBean>PizzaOrderBean = null;	
		try {
			Query query = entityManager.
					createQuery("select k from PizzaOrderEntity k where k.bill>=?1 and k.bill<=?2");
			query.setParameter(1, fromBill);
			query.setParameter(2, toBill);
			
			List<PizzaOrderEntity> listPizzaOrderEntity= query.getResultList();
					
			PizzaOrderBean=new ArrayList<PizzaOrderBean>();
			
			for (PizzaOrderEntity entity:listPizzaOrderEntity){
				PizzaOrderBean pizzaOrderBean= convertPizzaOrderEntityToBean(entity);
				PizzaOrderBean.add(pizzaOrderBean);
			}
		} catch (Exception exception) {
			throw exception;
		}
		return PizzaOrderBean;
	}
	public static PizzaOrderBean convertPizzaOrderEntityToBean(PizzaOrderEntity entity){
		PizzaOrderBean pizzaOrderBean = new PizzaOrderBean();
		BeanUtils.copyProperties(entity, pizzaOrderBean);
		return pizzaOrderBean;
	}
	

}
