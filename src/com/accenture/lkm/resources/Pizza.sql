DROP DATABASE IF EXISTS PizzaDetails;
CREATE DATABASE PizzaDetails; 
USE PizzaDetails;

DROP TABLE IF EXISTS Pizza;


CREATE TABLE IF NOT EXISTS Pizza (
  pizzaId int(11) unsigned NOT NULL AUTO_INCREMENT,
  pizzaName varchar(20) DEFAULT NULL,
  price double DEFAULT NULL,
  PRIMARY KEY (pizzaId)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO Pizza (pizzaId,pizzaName,price) VALUES
	(1001, 'XYZVegS',200),
	(1002, 'XYZVegM',400),
	(1003, 'XYZVegL',600),
	(1004, 'XYZNonVegS',400);


commit;