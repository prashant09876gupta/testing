DROP TABLE IF EXISTS Pizza_Order;
USE PizzaDetails;
CREATE TABLE IF NOT EXISTS Pizza_Order (
  orderId int(11) unsigned NOT NULL AUTO_INCREMENT,
  customerName varchar(30) DEFAULT NULL,
  contactNumber varchar(10) DEFAULT NULL,
  pizzaId int(11) REFERENCES Pizza(pizzaId),
  numberOfPiecesOrdered int(11) NOT NULL,
  bill double DEFAULT NULL,
  PRIMARY KEY (orderId)
) ENGINE=InnoDB AUTO_INCREMENT=5005 DEFAULT CHARSET=utf8;

INSERT INTO Pizza_Order (orderId,customerName,contactNumber,pizzaId,numberOfPiecesOrdered,bill) VALUES
	(5001 ,'Peter', '1234567890', 1001, 1, 200),
	(5002, 'Thomas', '6574893012', 1001, 2, 400),
	(5003, 'Decken', '1243568790', 1003, 1, 600),
	(5004, 'Jenifer', '1029384756', 1004, 2, 800);	
commit;