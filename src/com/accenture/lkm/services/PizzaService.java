package com.accenture.lkm.services;

import java.util.List;
import java.util.Map;

import com.accenture.lkm.bussiness.bean.PizzaOrderBean;

public interface PizzaService {
	public List<PizzaOrderBean> getOrderDetails(Double fromBill, Double toBill ) throws Exception;
	public PizzaOrderBean addPizzaOrderDetails(PizzaOrderBean PizzaOrder)throws Exception;
	public Map<Integer,String>findAllPizzaDetails() throws Exception;

}
