package com.accenture.lkm.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.lkm.bussiness.bean.PizzaBean;
import com.accenture.lkm.bussiness.bean.PizzaOrderBean;
import com.accenture.lkm.dao.PizzaDaoWrapper;

@Service
public class PizzaServiceImpl implements PizzaService {
	@Autowired
	private PizzaDaoWrapper pizzaDAOWrap;

	@Override
	public List<PizzaOrderBean> getOrderDetails(Double fromBill, Double toBill) throws Exception {
		// TODO Auto-generated method stub
		List<PizzaOrderBean>pizzaorder=pizzaDAOWrap.getOrderDetails(fromBill, toBill);
		if(pizzaorder==null || pizzaorder.size()==0)
		{
			throw new Exception("No Record found yet!!");
		}
		return pizzaorder;
	}

	@Override
	public PizzaOrderBean addPizzaOrderDetails(PizzaOrderBean PizzaOrder) throws Exception {
		// TODO Auto-generated method stub
		double price=pizzaDAOWrap.getPizzaPrice(PizzaOrder.getPizzaId());
		double bill=PizzaOrder.getNumberOfPiecesOrdered()*price;
		PizzaOrder.setBill(bill);
		
		return pizzaDAOWrap.addPizzaOrderDetails(PizzaOrder);
	}

	@Override
	public Map<Integer, String> findAllPizzaDetails() throws Exception {
		// TODO Auto-generated method stub
		List<PizzaBean>list=pizzaDAOWrap.findAllPizzaDetails();
		Map<Integer,String>pizzaMap=new HashMap<Integer,String>();
		for(PizzaBean pizza:list)
		{
			pizzaMap.put(pizza.getPizzaId(), pizza.getPizzaName());
		}
		
		return pizzaMap;
	}

}
