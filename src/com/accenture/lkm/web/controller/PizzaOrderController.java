package com.accenture.lkm.web.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.lkm.bussiness.bean.PizzaOrderBean;
import com.accenture.lkm.services.PizzaService;

@Controller
public class PizzaOrderController {
	@Autowired
	private PizzaService pizzaService;
	
	
	@RequestMapping(value="/loadPizzaDetail" ,method= RequestMethod.GET)
	public ModelAndView getPizzaDetails() throws Exception
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("PizzaOrder");
		modelAndView.addObject("PizzaOrderbean" ,new PizzaOrderBean());
		return modelAndView;
		
	}
	
	@RequestMapping(value="/saveOrder", method = RequestMethod.POST)
	public ModelAndView addPizzaDetails(@Valid @ModelAttribute("PizzaOrderbean")PizzaOrderBean pizzaOrderBean,BindingResult bindingResult) throws Exception
	{
		ModelAndView modelAndView=new ModelAndView();
		if(bindingResult.hasErrors())
		{
			modelAndView.setViewName("PizzaOrder");
		}
		else
		{
			PizzaOrderBean pizzaDetails=pizzaService.addPizzaOrderDetails(pizzaOrderBean);
			modelAndView.setViewName("PizzaOrderSuccess");
			modelAndView.addObject("message","Hi "+pizzaDetails.getCustomerName()+", your order is placed with orderId: "
					   +pizzaDetails.getOrderId()
					   +", Bill to be paid is: "+pizzaDetails.getBill());
			
			
		}
		return modelAndView;
		
		
	}
	@ModelAttribute("pizzaNamesAndId")
	public Map<Integer,String> populatePizzaNames() throws Exception{
		return pizzaService.findAllPizzaDetails();
	}
	//@ExceptionHandler(value=Exception.class)
	public ModelAndView handleAllExceptions(Exception exception){
		ModelAndView  modelAndView = new ModelAndView();
		modelAndView.setViewName("GeneralizedExceptionHandlerPage");
		modelAndView.addObject("message", exception.getMessage());
		return modelAndView;
	}
	

}
