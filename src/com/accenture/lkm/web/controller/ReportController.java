package com.accenture.lkm.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.lkm.bussiness.bean.BillRangeBean;
import com.accenture.lkm.bussiness.bean.PizzaOrderBean;
import com.accenture.lkm.services.PizzaService;

@Controller
public class ReportController {
	@Autowired
	private PizzaService pizzaService;
	
	@RequestMapping(value="/loadOrderWithinRange")
	public ModelAndView dateRangeDetails() throws Exception
	{
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("OrderReport");
		modelAndView.addObject("ReportWithinRange", new BillRangeBean());
		return modelAndView;
		
	}
	
	@RequestMapping(value="/OrderDetailsWithinRangeDate",method= RequestMethod.POST)
	public ModelAndView orderDetails(@ModelAttribute("ReportWithinRange") BillRangeBean billRangeBean)throws Exception
	{
		List<PizzaOrderBean>listPizzaorder= pizzaService.getOrderDetails(billRangeBean.getFromPrice(),billRangeBean.getToPrice());
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("OrderReport");
		modelAndView.addObject("OrderBill",listPizzaorder);
		return modelAndView;
		
	}
	@ExceptionHandler(value=Exception.class)
	public ModelAndView handleAllExceptions(Exception exception){
		ModelAndView  modelAndView = new ModelAndView();
		modelAndView.setViewName("GeneralizedExceptionHandlerPage");
		modelAndView.addObject("message", exception.getMessage());
		return modelAndView;
	}

}
